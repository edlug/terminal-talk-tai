. local_venv/bin/activate

presd="src-presentation"
presf="main.rst"

sed -r -e 's/%`/:code:`/g' "$presd/$presf" > "$presd/local_${presf}"

(sleep 1; python3 -m webbrowser http://localhost:8000) &

hovercraft "$presd/local_${presf}"

