:title: Terminal for Beginners
:css: css/styles.css

----

At Your Command
===============

Why & How Command Lines Help You
--------------------------------

* EdLUG September 2020
* Tai Kedzierski

https://gitlab.com/edlug/terminal-talk-tai

.. note::

    This presentation does not use presenters notes
    because I didn't have a secondary screen

----

Who am I
========

* Systems engineer
* Linux enthusiast since 2013
    * When it became easier??
* Terminal junkie

----

Where to find me

* %`dev.to/taikedz`
* %`fosstodon.org/@taikedz`
* %`edlug.gitlab.io`
* %`@taikedz`

----

.. image:: images/my-desktop.png
    :height: 600px
    :align: center

----

Why command lines?
==================

* The original way to use computers
* More expressive in data processing situations
* Shareable routines

----

.. image:: https://www.commitstrip.com/wp-content/uploads/2016/12/Strip-Lignes-de-commande-english650-final-2.jpg
    :align: center

----

Why command lines...
====================

... for you ?
-------------

* Easier to give commands than to explain GUIs
    * No making screenshots for help documents
* Online help often uses commands
* Avoid mice, avoid trackpads
* You can automate
* Navigate the web like it's 1984 :-)
* Scare people in cafes :-O

----

Expectations
============

* https://gitlab.com/edlug/terminal-talk-tai
    * https://tinyurl.com/edlug-2020-09
* Will go through the basics fast
* Lots of content, not to remember immediately
* More interesting stuff after covering them ;-)
* Other junkies : there are sometimes more advanced ways of doing things, I didn't include them.

----

Conventions
===========

.. code:: sh

    # comment

    $>   # The prompt - don't type it

    $> command argument1 argument2 # Two arguments
    $> command ARGUMENT  # ARGUMENT is to be replaced
         # by your own text
    $> command "single argument with spaces"  # One argument

    $> command [optional argument]

Some commands will talk about "dirs" or "directories." These are the same as "folders."

----

Open a command line
===================

* :code:`Ctrl + Alt + T`
* Right click in folder: :code:`Open in terminal`
* Start menu search: :code:`Terminal`

----

Basics
======

.. code:: sh

    $> pwd  # Show the current folder you are in
    $> ls   # Show the contents of the folder you are in
    $> cd   # Go to a different folder

    $> ls /   # Like `dir C:\` on Windows

----

Follow along
============

If you are following along, do this first:

.. code:: sh

    mkdir ~/demo
    cd ~/demo

If you think you are no longer in the demo folder at some point during this:

* check with %`pwd`
* Go back to the demo folder with %`cd ~/demo`

----

Globs
=====

The %`*` matches "anything"

%`*.txt` matches any files in the current folder that ends with %`.txt`

%`my-*` matches any file in the current folder that starts with %`my-`

.. code:: sh

    $> ls my-*

    $> ls *.txt

----

.. code:: sh

    # touch FILE : create an empty file
    # e.g.
    $> touch newfile.txt

    # mv OLD NEW
    # Also used for renaming files
    $> mv newfile.txt some-notes.txt

    # cp HERE THERE
    $> cp some-notes.txt copy-of-notes.txt

    # mkdir FOLDER
    $> mkdir my-notes
    $> mv *.txt my-notes/

    # rm [-r] FILE
    $> rm -r my-notes/

----

.. code:: sh

    $> echo hello

    $> echo "Some stuff I want to keep" > notes.txt

----

%`cat`, %`head`, %`tail`, %`less`

There are a lot of logs in %`/var/log/`

.. code:: sh

    # cat FILE1 FILE2 ...
    $> cat notes.txt

    $> ls /var/log

    # Last few lines of a file
    # tail FILE ...
    $> tail /var/log/auth.log

    # Use PageUp and PageDown to navigate
    # Use `/` to search
    $> less /var/log/auth.log

----

%`locate`

Find files by a part of their name. You probably want to **pipe**

.. code:: sh

    $> locate taxes

    $> locate .log | less

----

grep
====

A tool for finding something in a stream of text

.. code:: sh

    $> locate edlug | grep gitlab

    $> locate edlug | grep -v gitlab

----

grep
====

A tool for finding something by its content

Slow, because going through ALL THE FILES.

.. code:: sh

    # grep -r TEXT FOLDER ...
    $> grep -r "January" taxes/

----

Performance
===========

.. code:: sh

    # See what's using the most CPU
    # Press `m` to show most memory usage
    # Press `m` again to return to CPU usage
    # Press `q` to quit
    $> top

----

TTY
===

A special type of terminal can be activate by performing %`Ctrl + Alt + F[2-6]`

You can return to the graphical session via %`Ctrl + Alt + F7`

Full terminal mode. When graphical responsiveness is going right down!

----

KILL
====

Actually "send signal"

.. code:: sh

    # Send signal by PID
    $> kill 1234

    # Forcibly actually kill
    $> kill -9 1234

    # By name
    $> pkill chromium
    $> pkill -9 chromium

    # !!! Will kill any process with an "a" in its name!
    $> pkill a

----

Edit Files
==========

.. code:: sh

    # ^O = Ctrl+O -- save
    # # ^X = Ctrl+X -- exit
    # "M^" = Alt key
    $> nano FILE

----

Pause and Resume
================

You can pause a command that is currently running using %`Ctrl + Z`

You can resume a paused program by running %`fg`

You can force-quit a command using %`Ctrl + C`

You can see paused programs by running %`jobs`

You can resume a specific program by running %`fg N` where N is the number listed in %`jobs`

----

Super User
==========

* Sometimes you need admin privileges
* %`su - COMMAND` and %`sudo`
* More often, %`sudo COMMAND ...`

----

Install stuff
=============

.. code:: sh

    # Ubuntu, Debian, Mint, elementary
    apt search SOMETHING
    apt show THING
    sudo apt install THING

    # Fedora, CentOS and Red Hat 8+
    dnf search SOMETHING
    dnf info THING
    sudo dnf install THING

    # CentOS & Red Hat 7 and earlier
    yum search SOMETHING
    yum info THING
    sudo yum install THING

----

AND BREATHE
===========

Now we can look at fun things!

----

Some things you are sometimes asked to install

.. code:: sh

    # "You need Java"
    sudo apt install default-jdk

    # A nicer way to see `top` stuff
    sudo apt install htop

Try %`apt search game | less`

----

LibreOffice
===========

FOSS alternative to MS Office etc.

.. code:: sh

    $> libreoffice --headless --convert-to csv --outdir SOMEFOLDER *.xls
    $> grep "Total:" SOMEFOLDER/*.csv

.. code:: sh

    $> libreoffice --headless --convert-to pdf *.docx

----

Thumbnails
==========

Get ImageMagick:

    %`sudo apt install imagemagick`

Basic command:

.. code:: sh

    # convert -thumbnail WIDTH_SIZE ORIGINAL THUMBNAIL
    $> convert -thumbnail 200 abc.jpg thumb.abc.jpg

Convert lots:

.. code:: sh

    for myimage in *.jpg; do
        convert -thumbnail 200 "$myimage" "thumb.$myimage"
    done

----

Scripts
=======

Don't want to remember long commands? Make a script!

Setup (do this only once):

.. code:: sh

    mkdir -p "$HOME/.local/bin"
    echo 'export "$HOME/.local/bin"' >> ~/.bashrc
    exec bash

Making a script:

.. code:: sh

    $> nano ~/.local/bin/SCRIPTNAME
    # Then add your script
    $> chmod 755 ~/.local/bin/SCRIPTNAME

----

Example for a notes keeper - save into %`~/.local/bin/addnote.sh`

.. code:: sh

    cd DropBox/notes
    nano "$1"

%`"$1"` is a short hand to "the first argument to this script"

From a terminal, you can now immediately add a new note into your DropBox, or edit an existing one.

.. code:: ssh

    addnote.sh todo.txt

----

More stuff
==========

----

Convert audio files

.. code:: sh

    $> sudo apt install ffmpeg # Only once, to ensure it is installed
    # ffmpeg -i MYFILE OUTFILE.EXT

    $> ffmpeg -i mysong.flac mysong.mp3

How about a script?

.. code:: sh

    for myfile in "$@"; do
        ffmpeg -i "$myfile" "$myfile.mp3"
    done

%`"$@"` is "all arguments to this script"

----

YouTube Videos
==============

Install

.. code:: sh

    pip install --user youtube-dl # only once

Update

.. code:: sh

    pip install --user --upgrade youtube-dl

Actually use

.. code:: sh

    # youtube-dl YOUTUBEURL
    $> youtube-dl https://www.youtube.com/watch?v=q3RS73anKF8

    # youtube-dl "YOUTUBE_PLAYLIST" # Needs the quote marks
    # The "&" has a special meaning on the command line
    $> youtube-dl "https://www.youtube.com/watch?v=beZbjXlwGAA&list=PL-AaW_SPm-Bm-MPANntyyeH-LZVKRiJgQ"

----

...
===

----

Watermarks
==========

Publishing photos? You might want to add your watermark

.. code:: sh

    # composite -dissolve 30% -gravity south \
    #   WATERMARK_FILE INPUT_FILE OUTPUT_FILE

    $> composite -dissolve 30% -gravity south \
    $>   watermark.jpg mypic.jpg wm-mypic.jpg

Script:

.. code:: sh

    # Assign the first argument to a variable name...
    #   then remove it from the script overall arguments (`shift`)
    watermark="$1"; shift

    # Now "$@" is everything EXCEPT the first variable,
    #   because we said to forget it.
    for picture in "$@"; do
        composite -dissolve 30% -gravity south \
          "$watermark" "$picture" "wm-$picture"
    done

----

Copy Files
==========

You copy a folder by dragging it. Then the computer says "Error". And you need to resume the copy. Somehow. Bleh.

Or

.. code:: sh

    # rsync -av SOURCE_FOLDER/ DESTINATION_FOLDER/
    $> rsync -av "$HOME/" "/media/tai/USB_KEY/backups/tai"


That was an example of a backup. Here's an example of a backup restoration

.. code:: sh

    $> rsync -av "/media/tai/USB_KEY/backups/tai" "$HOME/"

For the %`rsync` command, those last %`/` characters are important !!!

----

Silly things
============

----

Make your computer talk!

Setup:

.. code:: sh

    sudo apt install espeak

Use it:

.. code:: sh

    espeak "I have done your bidding, master."

----

Weird sentences on terminals
============================

Setup:

.. code:: sh

    sudo apt install fortune lolcat cowsay
    echo "fortune|cowsay|lolcat" >> ~/.bashrc

* %`fortune` prints a silly sentence or quote
* %`cowsay` wraps that in a text-based picture of a cow
* %`lolcat` adds random rainbow colours

This will now activate every time you open a terminal...!

----

Browse the web old-style
========================

Setup:

.. code:: sh

    sudo apt install elinks

Usage:

.. code:: sh

    elinks https://wikipedia.org

----

Sharing Code
============

Use a pastebin

* https://pastebin.com
* https://gist.github.com

On Gist/Github:

* Give the file an appropriate name (%`*.sh` for command lines in Linux)
* Save
* Copy the link in the address bar and send it to people

----

Getting Help
============

* The %`man COMMAND_NAME` command gives you a manual
* Uses %`less` for navigation
* Or run the command with %`--help`: %`grep --help`
* Or use a search engine
* https://duckduckgo.com (privacy-respecting alternative to https://google.com )
* StackExchange and sister sites
    * https://askubuntu.com
    * https://superuser.com
    * https://stackoverflow.com

----

NEVER
=====

* %`rm -rf /` -- Erases as much as possible from computer, and any disk attached to it
* %`:(){:|:;}` -- Grind the computer to a halt

----

What if I don't have Linux?
===========================

There are command lines in Windows too!

* Cygwin - a Linux-like environment for Windows
* PowerShell - very different commands than Linux, but the base principles are similar
* Prepare to Duck a lot!

----

Conclusion
==========

That was a lot!

Reminder that you can get the notes here:

https://tinyurl.com/edlug-2020-09

Ask me questions through %`@taikedz` on your preferred platform (wll.... most of them....)
